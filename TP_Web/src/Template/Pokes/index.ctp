<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke[]|\Cake\Collection\CollectionInterface $pokes
 */
?>

<div class="pokes index large-9 medium-8 columns content">
    <h3><?= __('Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pokedex_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach ($pokes as $poke): ?>
            <tr>
                <td><?= $this->Number->format($poke->id) ?></td>
                <td><?= h($poke->name) ?></td>
                <td><?= $this->Number->format($poke->pokedex_number) ?></td>
                <td><?= h($poke->created) ?></td>
                <td><?= h($poke->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $poke->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $poke->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

    <?php foreach ($pokes as $poke): ?>
        <div class="row">
    <div class="col-sm"></div>
            <?php foreach ($pokes as $poke): ?>
                <div class="card" style="width: 18rem;">
                <div class="card-body">
                <h5 class="card-title"><?= h($poke->name) ?></h5>
                <h6 class="card-subtitle mb-2 text-muted">id : <?= $this->Number->format($poke->id) ?></h6>
                <p class="card-text">Pokedex number : <?= $this->Number->format($poke->pokedex_number) ?></p>
                <p class="card-text2"> Create : <?= h($poke->created) ?>
                modified <?= h($poke->modified) ?></p>
                <a href="#" class="card-link">Card link</a>
                <a href="#" class="card-link">Another link</a>
               
                </div>
             </div>
        <?php endforeach; ?>
</div>
  
            <?php endforeach; ?>