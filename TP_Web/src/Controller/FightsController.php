<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity(); //creation d'une nouvelle entité fight qui attend 3 paramètres (le combat, le premier dresseur et le second)
        if ($this->request->is('post')) { //requete soumis le formulaire on entre dans ce cas :
            $formData = $this->request->getData() ; //recuperer les données du formulaire
            $formData['winner_dresseur_id'] = $this->_retrieveFightWinner($formData);

            $fight = $this->Fights->patchEntity($fight, $formData); //recuper les données de mon formulaire, patchentity = mettre a jour avec les données de mon formulaire
            if ($this->Fights->save($fight)) { //si succès on passe ici sinon message d'erreur en dessous
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
       // $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs'));
    }

    protected function _retrieveFightWinner($formData)
    {
    
        $pokemon1 = 100;
        $pokemon2 = 100;
        do
        {
            $pokemon1 = $pokemon1-random_int(0,50);
            echo $pokemon1;
            $pokemon2 = $pokemon2-random_int(0,50);
            echo $pokemon1;

        }while(($pokemon1>0) || ($pokemon2>0));

        return $pokemon1 > $pokemon2 ? $formData['first_dresseur_id'] : $formData['second_dresseur_id'];
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function edit($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fight = $this->Fights->patchEntity($fight, $this->request->getData());
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }
    */
    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
